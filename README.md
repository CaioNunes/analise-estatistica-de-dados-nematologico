# Análise Estatística de Dados Nematológicos
#### Automatizar a entrada de dados e os cálculos dos experimentos da contaminação por nematoides com determinado tratamento, repetição e interpretar os resultados obtidos.
# Grupo
- [Carlos](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Carlos)<br><br>
- [Caio](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Caio)<br><br>
- [Gabriel (SCRUM)](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Gabriel)<br><br>
- [Marcos (SCRUM)](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Marcos)<br><br>
- [Michel](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Michel)<br><br>
- [Pedro](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Pedro)<br><br>
- [Thomas](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Thomas)<br><br>
- [Luan](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Luan)<br><br>

#### <strong>Veja nossas atividades [CLICANDO AQUI](https://gitlab.com/BDAg/analise-estatistica-de-dados-nematologico/wikis/Atividades)!</strong>

